package plugins.adufour.viewers;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import icy.canvas.Canvas2D;
import icy.canvas.IcyCanvas;
import icy.gui.frame.progress.ToolTipFrame;
import icy.gui.viewer.Viewer;
import icy.image.IcyBufferedImage;
import icy.painter.Overlay;
import icy.plugin.abstract_.PluginActionable;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.type.point.Point5D;
import icy.util.ColorUtil;

public class Magnifier extends PluginActionable
{
    @Override
    public void run()
    {
        Viewer viewer = getActiveViewer();

        if (viewer == null)
            throw new IcyHandledException("Open an image before using the magnifier");

        if (viewer.getSequence() == null)
            throw new IcyHandledException("The active viewer contains no sequence");

        if (viewer.getCanvas() instanceof Canvas2D)
        {
            viewer.getCanvas().addLayer(new MagnifierOverlay(viewer, 4));
        }
        else
            throw new IcyHandledException("The magnifier only works on the default 2D viewer");
    }

    public static final class MagnifierOverlay extends Overlay
    {
        protected final ToolTipFrame frame;

        protected float currentPosX, currentPosY;
        protected int currentChannel = 0;
        protected int radius = 50;
        protected int diameter = radius * 2;
        protected int scale;

        protected AffineTransform xForm = new AffineTransform();
        protected GradientPaint paint = new GradientPaint(0, 0, Color.black, radius, radius, Color.darkGray.darker(),
                true);
        protected Ellipse2D.Float lens = new Ellipse2D.Float();
        protected Font font;

        public MagnifierOverlay(Viewer viewer, int magnifierScale)
        {
            super("Magnifier");
            this.currentChannel = viewer.getSequence().getSizeC();
            setScale(magnifierScale);

            String howto = "<b>The magnifier is now active on the current viewer</b><br/>";
            howto += "To inspect pixel values, zoom in and press 'c' to toggle between channels.<br/>";
            howto += "Finally, press 'Escape' to deactivate the magnifier (and close this box)";
            frame = new ToolTipFrame(howto);
        }

        public void setScale(int scale)
        {
            this.scale = scale;
            font = new Font("Tahoma", Font.PLAIN, scale).deriveFont(scale * 0.1f);
        }

        @Override
        public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
        {
            if (!(canvas instanceof Canvas2D))
                return;

            float xo = currentPosX - radius;
            float yo = currentPosY - radius;

            float currentZoom = (float) canvas.getScaleX();
            boolean displayValues = currentChannel < sequence.getSizeC() && currentZoom >= 2f;

            // draw the lens
            Graphics2D graphics = (Graphics2D) g.create();
            graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
            graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            graphics.setPaint(paint);
            graphics.setStroke(new BasicStroke(scale));
            graphics.draw(lens);

            // draw the "zoomed" image inside the lens
            graphics.clip(lens);
            // graphics.setTransform(xForm);
            graphics.translate(xForm.getTranslateX(), xForm.getTranslateY());
            graphics.scale(scale, scale);
            
            // paint image pixels into the lens
            canvas.getImageLayer().getOverlay().paint(graphics, sequence, canvas);

            // draw pixel values
            if (displayValues)
            {
                // set the text's color/font/size
                graphics.setColor(Color.white);
                graphics.setFont(font);
                FontMetrics metrics = graphics.getFontMetrics();
                float fontHeight = metrics.getHeight() - 1.35f;

                // set transparency to depend on the zoom factor
                graphics.setComposite(
                        AlphaComposite.getInstance(AlphaComposite.SRC_OVER, Math.min(1f, (currentZoom - 2f) / 1.5f)));

                IcyBufferedImage im = canvas.getCurrentImage();
                BufferedImage finalImage = ((Canvas2D) canvas).getCanvasView().getImageCache().getImage();
                Color baseColor = im.getColorMap(currentChannel).getDominantColor();
                Color darkColor = baseColor.darker().darker();
                Color brightColor;

                // making the base color brighter works in most cases, but not for blue
                // => to make blue "look" brighter, mix it cyan
                if (ColorUtil.getDistance(baseColor, Color.white, true) > 0.2 && baseColor.getBlue() > 128)
                    brightColor = ColorUtil.mix(baseColor, Color.cyan).brighter().brighter();
                else
                    brightColor = baseColor.brighter().brighter();

                for (int j = (int) yo; j < yo + diameter; j++)
                {
                    for (int i = (int) xo; i < xo + diameter; i++)
                    {
                        if (im.getBounds().contains(i, j))
                        {
                            int val = (int) im.getData(i, j, currentChannel);
                            String s = (val < 10 ? "   " : val < 100 ? "  " : val < 1000 ? " " : "") + val;
                            int rgb = finalImage.getRGB(i, j);
                            int lum = ColorUtil.getLuminance(new Color(rgb));
                            graphics.setColor(lum < 128 ? brightColor : darkColor);
                            graphics.drawString(s, i + (0.2f / s.length()), j + fontHeight);
                        }
                    }
                }
            }

            graphics.dispose();
        }

        @Override
        public void mouseMove(MouseEvent e, Point5D.Double imagePoint, IcyCanvas canvas)
        {
            if (!(canvas instanceof Canvas2D))
                return;

            currentPosX = (float) imagePoint.getX();
            currentPosY = (float) imagePoint.getY();
            xForm.setToTranslation(-currentPosX * scale + currentPosX, -currentPosY * scale + currentPosY);
            lens.setFrame(currentPosX - radius, currentPosY - radius, diameter, diameter);

            canvas.repaint();

            super.mouseMove(e, imagePoint, canvas);
        }

        @Override
        public void keyPressed(KeyEvent e, Point2D imagePoint, IcyCanvas canvas)
        {
            if (e.getKeyChar() == 'c')
            {
                currentChannel = (currentChannel + 1) % (canvas.getCurrentImage().getSizeC() + 1);
                canvas.repaint();
            }
            else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
            {
                remove();
                canvas.removeLayer(this);
                canvas.refresh();
            }
        }

        @Override
        public void remove()
        {
            frame.close();
            super.remove();
        }
    }
}
